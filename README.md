# jproc - Java source processing tool

**jproc** is a [JavaParser](https://javaparser.org) based tool which enables users to use Groovy scripts in order to 
manipulate their Java sources.

This project uses the [ShadowJar](https://github.com/johnrengelman/shadow) Gradle plugin to create *all-in-one* JAR with all dependencies.

## Synopsis

Linux:
```
$ git clone https://bitbucket.org/mbazs/jproc
$ cd jproc
$ ./gradlew shadowJar
$ java -jar app/build/libs/app-1.0.0-all.jar proc -S examples/javaparser_metamodels.groovy -R testpackage | less  
```

Windows:
```
git clone https://bitbucket.org/mbazs/jproc
cd jproc
gradlew.bat shadowJar
java -jar app\build\libs\app-1.0.0-all.jar proc -S examples\javaparser_metamodels.groovy -R testpackage | more
``` 

## Usage

This tool is a command line interface based Java tool, which can be used like this:

 - Prints help:  
`./gradlew run --args 'help'`

 - Prints help of the (currently only) `proc` subcommand:  
`./gradlew run --args 'help proc'`

 - Executes `proc` subcommand recursively on the `testpackage` subdirectory (this WILL work on current repo):  
`./gradlew run --args 'proc -S ../examples/javaparser_metamodels.groovy -R ../testpackage'`

# testpackage

`testpackage` is a project-internal Java package which aim is to be able to test this tool conveniently.
