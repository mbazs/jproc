/**
 * Demo of JavaParser's language constructs
 */

import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.expr.MethodCallExpr
import com.github.javaparser.ast.expr.VariableDeclarationExpr
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade

def process(String filename, CompilationUnit cu, JavaParserFacade jpf) {
    cu.findAll(VariableDeclarationExpr).forEach(expr -> {
        var resolvedType = jpf.getType(expr)
        println "var decl: ${expr.toString()} -> ${resolvedType}"
        if (resolvedType.isReferenceType()) {
            Class.forName(jpf.getType(expr).describe())
            println "class ${resolvedType.describe()} loaded successfully"
        }
        println ""
    })

    cu.findAll(MethodCallExpr).forEach(callExpr -> {
        println "method call: ${callExpr}"
        println "return type: ${callExpr.calculateResolvedType().describe()}"
        println "arguments:"
        callExpr.getArguments().forEach(arg -> {
            println "${arg} -> ${arg.calculateResolvedType().describe()}"
        })
        println ""
    })
}
