import com.github.javaparser.ast.CompilationUnit
import static com.ms.jproc.Utils.*
import com.github.javaparser.ast.body.MethodDeclaration
import com.github.javaparser.ast.comments.LineComment
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade

def process(String filename, CompilationUnit cu, JavaParserFacade jpf) {
    specSectionCommentStart = "--- SPECIAL SECTION BEGIN - DO NOT TOUCH THIS COMMENT ---"
    specSectionCommentEnd   = "--- SPECIAL SECTION END   - DO NOT TOUCH THIS COMMENT ---"

    cu.addImport(java.io.File.class)
    cu.findAll(MethodDeclaration).each {
        println 'processing method ' + it.name
        body = it.getBody().get()
        if (!body.getAllContainedComments().stream().anyMatch(c -> c.getContent().contains(specSectionCommentStart))) {
            body.addStatement("System.out.println(\"Hello World at method '" + it.name + "'\"); // " + specSectionCommentStart)
                .addStatement("System.out.println(\"more logging ...\");")
                .addOrphanComment(new LineComment(specSectionCommentEnd))
        }
    }
    save(cu)
}
