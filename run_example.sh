#!/bin/bash
SCRIPT=basic

if [[ ! -z "$1" ]]; then
  SCRIPT=$1
fi

./gradlew run --args "proc -S ../examples/$SCRIPT.groovy -R ../testpackage"
