package com.ms.jproc;

import com.github.rvesse.airline.annotations.Cli;
import com.github.rvesse.airline.help.Help;

@Cli(
    name="jproc",
    defaultCommand = Help.class,
    commands = {
        Help.class,
        Processor.class
    }
)
public class App {
    public static void main(String[] args)
    {
        com.github.rvesse.airline.Cli<Runnable> appCli = new com.github.rvesse.airline.Cli<>(App.class);
        try {
            appCli.parse(args).run();
        } catch (Exception e) {
            System.err.println(e.getMessage() + "\nExiting.\n");
            System.exit(1);
        }
    }
}
