package com.ms.jproc;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.github.rvesse.airline.annotations.Arguments;
import com.github.rvesse.airline.annotations.Command;
import com.github.rvesse.airline.annotations.Option;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Command(
    name="proc",
    description = "process java sources"
)
public class Processor implements Runnable {
    @Option(
        name = { "-R", "--recursive" },
        description = "recursive processing of sources",
        hidden = false
    )
    private boolean recursive = false;

    @Option(
            name = { "-S", "--script" },
            description = "processing script [jproc.groovy]",
            hidden = false
    )
    private String script = "jproc.groovy";

    @Arguments(
        description = "src: source path to be processed [.]",
        title = {"src"}
    )
    private String src = ".";

    private GroovyShell gshell = new GroovyShell();
    private Script gscript = null;

    @Override
    public void run() {
        try {
            gscript = gshell.parse(new File(script));
            processPath(new File(src));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processPath(File path) throws IOException {
        File[] files = path.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    if (file.getName().endsWith(".java")) {
                        processFile(file);
                    }
                } else {
                    if (file.isDirectory() && !Files.isSymbolicLink(file.toPath()) && recursive) {
                        processPath(new File(path.getAbsolutePath() + "/" + file.getName()));
                    }
                }
            }
        }
    }

    private void processFile(File file) throws IOException {
        CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
        combinedTypeSolver.add(new ReflectionTypeSolver());
        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(combinedTypeSolver);
        ParserConfiguration parserConf = new ParserConfiguration().setSymbolResolver(symbolSolver);
        JavaParser parser = new JavaParser(parserConf);

        gscript.invokeMethod("process", new Object[]{
            file.getAbsolutePath(),
            parser.parse(file).getResult().get(),
            JavaParserFacade.get(combinedTypeSolver)
        });
    }
}
