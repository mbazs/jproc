package com.ms.jproc;

import com.github.javaparser.ast.CompilationUnit;

public abstract class Utils {
    public static void save(CompilationUnit cu) {
        cu.getStorage().get().save();
    }
}
