import java.io.File;

public class Main {

    private static class MainPriv {
        private static class MainPriv2 {
            private static class MainPriv3 {
                private File file;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Hi, this is the main() method");
        // calling protected test0
        test0();
    }

    protected void test0() {
        System.out.println("Hi, this is the test0() protected method");
        // calling private test1
        test1();
    }

    private void test1() {
        System.out.println("Hi, this is the test1() private method");
    }
}
